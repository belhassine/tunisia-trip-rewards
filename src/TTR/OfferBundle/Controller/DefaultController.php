<?php

namespace TTR\OfferBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('TTROfferBundle:Default:index.html.twig', array('name' => $name));
    }
}
