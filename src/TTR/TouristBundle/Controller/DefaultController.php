<?php

namespace TTR\TouristBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use JMS\Serializer\SerializerBuilder;
use Symfony\Component\HttpFoundation\JsonResponse;
use DateTime;
class DefaultController extends Controller
{
    /**
     * @Route("/api/tourist/create")
     * @Method("POST")
     */
    public function createAction(Request $request)
    {
        
        $firstname = $request->request->get('firstname');
        $lastname = $request->request->get('lastname');
        $age = $request->request->get('age');
        $nationality = $request->request->get('nationality');
        $job = $request->request->get('job');
        $login = $request->request->get('login');
        $password = $request->request->get('password');
        
        $tourist = new \TTR\TouristBundle\Entity\Tourist();
        $tourist->setFirstname($firstname);
        $tourist->setLastname($lastname);
        $tourist->setAge($age);
        $tourist->setNationality($nationality);
        $tourist->setJob($job);
        $tourist->setLogin($login);
        $tourist->setPassword(sha1($password));
        
        $em = $this->getDoctrine()->getManager();
        $em->persist($tourist);
        $em->flush();
        $tourist = $em->getRepository('TTRTouristBundle:Tourist')->findTouristById($tourist->getId());
        
        
//        $serializer = SerializerBuilder::create()->build();
        //$jsonContent = $serializer->serialize($lastConfig, 'json');
        $jsonContent = json_encode(array('success' => 'true','id' => $tourist->getId()));
        
        $response = new JsonResponse($jsonContent);
        $response->setContent($jsonContent);
        $response->setCharset("UTF-8");

        return $response;
    }
    
    /**
     * @Route("/api/tourist/login")
     * @Method("POST")
     */
    public function loginAction(Request $request)
    {
        $login = $request->request->get('login');
        $password = $request->request->get('password');
        
        $em = $this->getDoctrine()->getManager();
        
        $id = $em->getRepository('TTRTouristBundle:Tourist')->authenticate($login,$password);
        
        if ($id != -1) {
            
            $response = new Response(json_encode(array('success'=>true,'id' => $id)));
            $response->headers->set('Content-Type', 'application/json; charset=UTF-8');
            
        }
        else {
            
            $response = new Response(json_encode(array('success'=>false)));
            $response->headers->set('Content-Type', 'application/json; charset=UTF-8');
            
        }
        
        
        
        return $response;
    }
    
    /**
     * @Route("/api/survey/all")
     * @Method("GET")
     */
    public function surveysListAction(Request $request)
    {
        
        $em = $this->getDoctrine()->getManager();
        $surveys = $em->getRepository('TTRSurveyBundle:Survey')->findAll();
        
        $serializer = SerializerBuilder::create()->build();
        $jsonContent = $serializer->serialize($surveys, 'json');
        
        $response = new JsonResponse($jsonContent);
        $response->setContent($jsonContent);
        $response->setCharset("UTF-8");
        
        
        return $response;
    }

    
    /**
     * @Route("/api/survey/submit")
     * @Method("POST")
     */
    public function submitsurveyAction(Request $request)
    {
        
        $em = $this->getDoctrine()->getManager();
        
        $hits = $request->request->get('hits');
        $tourist_id = $request->request->get('tourist_id');
        $survey_id = $request->request->get('survey_id');
        
 
        
        $hits_array = json_decode($hits);
        
        
        
        foreach ($hits_array as $hit) {

            $dbhit = new \TTR\SurveyBundle\Entity\SurveyAnswerHit();
            $dbhit->setSubmitdate(new DateTime());
            $answer = $em->getRepository('TTRSurveyBundle:SurveyAnswer')->find($hit->answer_id);
            $dbhit->setAnswer($answer);
            $tourist = $em->getRepository('TTRTouristBundle:Tourist')->find($hit->tourist_id);
            $dbhit->setTourist($tourist);
            $em->persist($dbhit);
            $em->flush();
        }
        
        $tourist = $em->getRepository('TTRTouristBundle:Tourist')->find($tourist_id);
        $survey = $em->getRepository('TTRSurveyBundle:Survey')->find($survey_id);
        
        $tourist->setPoints($tourist->getPoints()+$survey->getPoints());
        $em->persist($tourist);
        $em->flush();
        
        
        
        $response = new Response(json_encode(array('success'=>true,'points' =>$tourist->getPoints())));
        $response->headers->set('Content-Type', 'application/json; charset=UTF-8');
        
        
        return $response;
    }
    
    /**
     * @Route("/api/offer/all")
     * @Method("GET")
     */
    public function offersListAction(Request $request)
    {
         
        $em = $this->getDoctrine()->getManager();
        $offers = $em->getRepository('TTROfferBundle:Offer')->findAll();
        
        $serializer = SerializerBuilder::create()->build();
        $jsonContent = $serializer->serialize($offers, 'json');
        
        $response = new JsonResponse($jsonContent);
        $response->setContent($jsonContent);
        $response->setCharset("UTF-8");
        
        
        return $response;
        
    }
    
    /**
     * @Route("/api/offer/buy")
     * @Method("POST")
     */
    public function buyOfferAction(Request $request)
    {
         
        $em = $this->getDoctrine()->getManager();
        
        
        $tourist_id = $request->request->get('tourist_id');
        $offer_id = $request->request->get('offer_id');
        
        $tourist = $em->getRepository('TTRTouristBundle:Tourist')->find($tourist_id);
        $offer = $em->getRepository('TTROfferBundle:Offer')->find($offer_id);
        
        $tourist->setPoints($tourist->getPoints()-$offer->getPrice());
        
        $em->persist($tourist);
        $em->flush();
        
        $response = new Response(json_encode(array('success'=>true,'points' =>$tourist->getPoints())));
        $response->headers->set('Content-Type', 'application/json; charset=UTF-8');
        
        
        return $response;
        
    }
}
