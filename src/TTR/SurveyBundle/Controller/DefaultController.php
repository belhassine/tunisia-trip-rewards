<?php

namespace TTR\SurveyBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('TTRSurveyBundle:Default:index.html.twig', array('name' => $name));
    }
}
