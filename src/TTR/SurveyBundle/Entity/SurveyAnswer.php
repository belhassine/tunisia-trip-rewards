<?php

namespace TTR\SurveyBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SurveyAnswer
 *
 * @ORM\Table(name="survey_answer")
 * @ORM\Entity(repositoryClass="TTR\SurveyBundle\Repository\SurveyAnswerRepository")
 */
class SurveyAnswer
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;


    /**
     * @ORM\ManyToOne(targetEntity="SurveyQuestion", inversedBy="answers")
     * @ORM\JoinColumn(name="question_id", referencedColumnName="id")
     */
    private $question;
    
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return SurveyAnswer
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }



    /**
     * Set question
     *
     * @param \TTR\SurveyBundle\Entity\SurveyQuestion $question
     *
     * @return SurveyAnswer
     */
    public function setQuestion(\TTR\SurveyBundle\Entity\SurveyQuestion $question = null)
    {
        $this->question = $question;

        return $this;
    }

    /**
     * Get question
     *
     * @return \TTR\SurveyBundle\Entity\SurveyQuestion
     */
    public function getQuestion()
    {
        return $this->question;
    }
}
