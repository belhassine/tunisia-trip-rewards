<?php

namespace TTR\SurveyBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SurveyAnswerHit
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="TTR\SurveyBundle\Entity\SurveyAnswerHitRepository")
 */
class SurveyAnswerHit
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="submitdate", type="date")
     */
    private $submitdate;

    /**
     * @ORM\ManyToOne(targetEntity="SurveyAnswer")
     * @ORM\JoinColumn(name="answer_id", referencedColumnName="id")
     */
    private $answer;
    
    /**
     * @ORM\ManyToOne(targetEntity="TTR\TouristBundle\Entity\Tourist")
     * @ORM\JoinColumn(name="tourist_id", referencedColumnName="id")
     */
    private $tourist;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set submitdate
     *
     * @param \DateTime $submitdate
     *
     * @return SurveyAnswerHit
     */
    public function setSubmitdate($submitdate)
    {
        $this->submitdate = $submitdate;

        return $this;
    }

    /**
     * Get submitdate
     *
     * @return \DateTime
     */
    public function getSubmitdate()
    {
        return $this->submitdate;
    }

    /**
     * Set answer
     *
     * @param \TTR\SurveyBundle\Entity\SurveyAnswer $answer
     *
     * @return SurveyAnswerHit
     */
    public function setAnswer(\TTR\SurveyBundle\Entity\SurveyAnswer $answer = null)
    {
        $this->answer = $answer;

        return $this;
    }

    /**
     * Get answer
     *
     * @return \TTR\SurveyBundle\Entity\SurveyAnswer
     */
    public function getAnswer()
    {
        return $this->answer;
    }

    /**
     * Set tourist
     *
     * @param \TTR\TouristBundle\Entity\Tourist $tourist
     *
     * @return SurveyAnswerHit
     */
    public function setTourist(\TTR\TouristBundle\Entity\Tourist $tourist = null)
    {
        $this->tourist = $tourist;

        return $this;
    }

    /**
     * Get tourist
     *
     * @return \TTR\TouristBundle\Entity\Tourist
     */
    public function getTourist()
    {
        return $this->tourist;
    }
}
