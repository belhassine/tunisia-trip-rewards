<?php

namespace TTR\SurveyBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SurveyQuestion
 *
 * @ORM\Table(name="survey_question")
 * @ORM\Entity(repositoryClass="TTR\SurveyBundle\Repository\SurveyQuestionRepository")
 */
class SurveyQuestion
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="answertype", type="string", length=255)
     */
    private $answertype;

    
    /**
     * @ORM\ManyToOne(targetEntity="Survey", inversedBy="questions")
     * @ORM\JoinColumn(name="survey_id", referencedColumnName="id")
     */
    private $survey;

    /**
     * @ORM\OneToMany(targetEntity="SurveyAnswer", mappedBy="question")
     */
    private $answers;
    
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return SurveyQuestion
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set answertype
     *
     * @param string $answertype
     *
     * @return SurveyQuestion
     */
    public function setAnswertype($answertype)
    {
        $this->answertype = $answertype;

        return $this;
    }

    /**
     * Get answertype
     *
     * @return string
     */
    public function getAnswertype()
    {
        return $this->answertype;
    }


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->answers = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add answer
     *
     * @param \TTR\SurveyBundle\Entity\SurveyAnswer $answer
     *
     * @return SurveyQuestion
     */
    public function addAnswer(\TTR\SurveyBundle\Entity\SurveyAnswer $answer)
    {
        $this->answers[] = $answer;

        return $this;
    }

    /**
     * Remove answer
     *
     * @param \TTR\SurveyBundle\Entity\SurveyAnswer $answer
     */
    public function removeAnswer(\TTR\SurveyBundle\Entity\SurveyAnswer $answer)
    {
        $this->answers->removeElement($answer);
    }

    /**
     * Get answers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAnswers()
    {
        return $this->answers;
    }
    
    public function __toString() {
        return $this->title;
    }

    /**
     * Set survey
     *
     * @param \TTR\SurveyBundle\Entity\Survey $survey
     *
     * @return SurveyQuestion
     */
    public function setSurvey(\TTR\SurveyBundle\Entity\Survey $survey = null)
    {
        $this->survey = $survey;

        return $this;
    }

    /**
     * Get survey
     *
     * @return \TTR\SurveyBundle\Entity\Survey
     */
    public function getSurvey()
    {
        return $this->survey;
    }
}
